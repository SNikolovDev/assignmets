﻿namespace ConsoleApp1
{
    internal class Program
    {
        public class Person
        {
            public string Name { get; set; }

            public Person(string name)
            {
                this.Name = name;
            }
        }

        static void Main(string[] args)
        {
            int number = 5;
            Person person = new Person("Ivan");
            Method(number, person);
            Console.WriteLine(number);
            Console.WriteLine(person.Name);
        }

        static void Method(int number, Person person)
        {
            number = 2;
            person.Name = "Gosho";
            person = new Person("Misho");
            Console.WriteLine(person.Name);
            person.Name = "Tisho";
            Console.WriteLine(person.Name);
        }
    }
}









//5
//Gosho