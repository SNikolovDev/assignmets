﻿namespace _8._ConsoleApp4
{
    internal class Program
    {
        public class Parent
        {
            public string Name { get; }
            public int Age { get; }

            public Parent(string name, int age)
            {
                this.Name = name;
                this.Age = age;
            }
        }

        public class Child : Parent
        {
            public string Address { get; }

            public Child(string name, int age, string address)
                : base(name, age)
            {
                this.Address = address;
            }

            public override string ToString()
            {
                return this.Name + " " + base.Age + " " + this.Address;
            }
        }

        static void Main(string[] args)
        {
            Child child = new Child("Pesho", 33, "Sofia");
            Console.WriteLine(child);
        }
    }
}


//Pesho 33 Sofia