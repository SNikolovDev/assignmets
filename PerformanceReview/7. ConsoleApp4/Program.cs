﻿namespace _7._ConsoleApp4
{
	public class Feedback
	{
		private readonly List<string> comments = new List<string>();

		public IList<string> Comments
		{
			get
			{
				return new List<string>(this.comments);
			}
		}

		public void AddComment(string comment)
		{
			if (!string.IsNullOrEmpty(comment))
			{
				this.Comments.Add(comment);//
			}
		}
	}
	internal class Program
	{
		static void Main(string[] args)
		{
			var feedback = new Feedback();
			feedback.AddComment("Great job!");
			feedback.AddComment("Perfect");
			feedback.AddComment("Meh");

			foreach (var comment in feedback.Comments)
			{
                Console.WriteLine(comment);
            }
		}
	}
}
