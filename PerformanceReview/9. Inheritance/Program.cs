﻿namespace InheritanceAndPolymorphism
{
	internal class Program
	{
		static void Main(string[] args)
		{
			var a = new A();
			Console.WriteLine();
			var b = new B();
			Console.WriteLine();
			var c = new C();

			Car car = new Car("Opel", "Astra", 140);

            Console.WriteLine(new string('-', 20));

            car.Drive();//Using the method from the parent class;
			car.Honk();
		}
	}

	public abstract class Vehicle
	{
		public Vehicle(string brand, string model, double hp)
		{
			this.Brand = brand;
			this.Model = model;
			this.HorsePowers = hp;
		}

		public string Brand { get; set; }

		public string Model { get; set; }

		public double HorsePowers { get; set; }

		public int WheelsCount { get; set; }

		public void Drive()
		{
			Console.WriteLine("Driving.");
		}

		public abstract void Honk(); //Abstract method only in abstract class, no body;

		public virtual int ReturnWheelsCount()
		{
			return this.WheelsCount;
		}
	}

	public class Car : Vehicle
	{
		private const int wheelsCount = 4;

		public Car(string brand, string model, double hp)
			: base(brand, model, hp)
		{
		}

		public override void Honk() //It's mandatory to implement abstract base class;
		{
            Console.WriteLine($"The {this.GetType().Name} is honking."); ;
		}

		public override int ReturnWheelsCount()//It's not mandatory to be implemented, but it is preferably override or hide it;
		{
			return wheelsCount;
		}
	}


	public class A
	{
		public A()
		{
			Console.WriteLine("A");
		}
	}

	public class B : A
	{
		public B()
		{
			Console.WriteLine("B");
		}
	}

	public class C : B
	{
		public C()
		{
			Console.WriteLine("C");
		}
	}
}