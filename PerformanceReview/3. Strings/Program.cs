﻿using System.Runtime.CompilerServices;

namespace _3._Strings
{
	internal class Program
	{
		static void Main(string[] args)
		{
			//string str1 = "Telerik Academy";
			//string str2 = "Telerik Academy";
			//string str3 = new string(str1);
			//string str4 = new string("Telerik Academy");
			//Console.WriteLine(str1 == str2);
			//Console.WriteLine(str1 == str3);
			//Console.WriteLine(str3 == str4);
			//Console.WriteLine();
			//var text = "Telerik Academy1";

			//string str5 = text.Substring(0, 15);
			//Console.WriteLine(str1.Equals(str5));//??
			//Console.WriteLine(str1 == str5);
			//Console.WriteLine(str5);



			string original = "text";
			string newText = "text1";
			string substring = newText.Substring(0, 4);

			Console.WriteLine(new string('-', 20));

			Console.WriteLine(substring);

			Console.WriteLine(new string('-', 20));

			Console.WriteLine(original == substring);
			Console.WriteLine(original.Equals(substring));
		}
	}
}
























//True
//True
//True
//True