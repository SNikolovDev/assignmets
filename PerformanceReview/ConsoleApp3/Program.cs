﻿using System.Threading.Channels;

namespace ConsoleApp3
{
    internal class Program
    {
        public class Base
        {
            public virtual void Print()
            {
                Console.WriteLine("Base");
            }
        }

        public class Child : Base
        {
            public override void Print()
            {
                Console.WriteLine("Child");
            }
        }

        static void Main(string[] args)
        {
            Base baseRef = new Child();
            baseRef.Print();
        }
    }
}




//Child