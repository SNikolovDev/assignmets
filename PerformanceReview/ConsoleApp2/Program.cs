﻿namespace ConsoleApp2
{
    internal class Program
    {
        public class Person
        {
            public string Name { get; set; }

            public Person(string name)
            {
                this.Name = name;
            }
        }

        static void Main(string[] args)
        {
            Person p1 = new Person("Ivan");
            Person p2 = new Person("Ivan");

            Console.WriteLine(p1 == p2);
            Console.WriteLine(p1.Equals(p2));
        } 
    }
}



//False
//False
