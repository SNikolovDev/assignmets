﻿namespace Inheritance_Example
{
	internal class Program
	{
		static void Main(string[] args)
		{
			var car = new Car();
			var bus = new Bus();
			var carriage = new Carriage();

			var list = new List<Vehicle>();

			list.Add(car);
			list.Add(bus);
			list.Add(carriage);

			List<IHaveWheels> list2 = new List<IHaveWheels>();

			list2.Add(car);
			list2.Add(bus);
			list2.Add(carriage);
		}
	}

	public abstract class Vehicle : IHaveWheels
	{
		public abstract string Wheels { get; set; }

		public abstract void Drive();
	}

	public class Car : Vehicle, IHaveEngine
	{
		public string Engine { get; set; }
		public override string Wheels { get; set; }

		public override void Drive()
		{
			throw new NotImplementedException();
		}
	}
	public class Bus : Vehicle, IHaveEngine
	{
		public string Engine { get; set; }
		public override string Wheels { get; set; }

		public override void Drive()
		{
			throw new NotImplementedException();
		}
	}

	public class Carriage : IHaveWheels
	{
		public string Wheels { get; set; }
	}

	public interface IHaveWheels
	{
		public string Wheels { get; set; }
	}

	public interface IHaveEngine
	{
		public string Engine { get; set; }
	}
}