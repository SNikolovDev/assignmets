﻿namespace _6._ConsoleApp4
{
    public sealed class Task
    {
        private readonly string title;

        public Task(string title)
        {
            this.title = title;
        }

        public void ChangeTitle(string newTitle)
        {
            title = newTitle;
        }

        public override string ToString()
        {
            return title;
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Task task = new Task("Write code!");
            task.ChangeTitle("Done!");
            Console.WriteLine(task);
        }
    }
}