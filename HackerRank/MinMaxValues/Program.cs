﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;
using System.Runtime.CompilerServices;
using System.Numerics;

class Result
{
    /*
   * Complete the 'miniMaxSum' function below.
   *
   * The function accepts INTEGER_ARRAY arr as parameter.
   */

    public static void miniMaxSum(List<long> arr)
    {

        BigInteger max = long.MinValue;
        BigInteger min = long.MaxValue;

        for (int i = 0; i < 5; i++)
        {
            var current = arr[i];
            arr[i] = 0;
            BigInteger sum = arr.Sum();

            if (sum > max)
            {
                max = arr.Sum();
            }
            if (sum < min)
            {
                min = arr.Sum();
            }

            arr[i] = current;
        }

        Console.WriteLine($"{min} {max}");
    }
}

class Solution
{
    public static void Main(string[] args)
    {

        List<long> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt64(arrTemp)).ToList();

        Result.miniMaxSum(arr);
    }
}
