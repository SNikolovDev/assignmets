﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    /*
     * Complete the 'plusMinus' function below.
     *
     * The function accepts INTEGER_ARRAY arr as parameter.
     */

    public static void plusMinus(List<int> arr)
    {
        double positive = 0;
        double negative = 0;
        double zero = 0;
        var arrCount = arr.Count;

        foreach (var item in arr)
        {
            if (item > 0)
            {
                positive++;
            }
            else if (item < 0)
            {
                negative++;
            }
            else
            {
                zero++;
            }
        }

        var positiveRatio = positive / arrCount;
        var negativeRatio = negative / arrCount;
        var zeroRatio = zero / arrCount;            

        Console.WriteLine($"{positiveRatio:F6}");
        Console.WriteLine($"{negativeRatio:F6}");
        Console.WriteLine($"{zeroRatio:F6}");
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        Result.plusMinus(arr);
    }
}
