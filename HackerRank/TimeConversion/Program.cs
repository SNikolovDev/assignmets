﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    /*
     * Complete the 'timeConversion' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts STRING s as parameter.
     */

    public static string timeConversion(string s)
    {
        var result = string.Empty;
        var hour = s.Substring(0, 2).ToString();
        var secondPart = s.Substring(2, 6).ToString();

        if (s.EndsWith("PM") && hour != "12")
        {
            var hourInt = int.Parse(hour) + 12;
            result = (hourInt).ToString() + secondPart;
        }
        else if (s.EndsWith("PM") && hour == "12")
        {
            result = "00" + secondPart;
        }
        else
        {
            result = s.Remove(8);
        }

        return result;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("Windows"), true);

        string s = Console.ReadLine();

        string result = Result.timeConversion(s);
        Console.WriteLine(result);
        //textWriter.WriteLine(result);

        //textWriter.Flush();
        //textWriter.Close();
    }
}
