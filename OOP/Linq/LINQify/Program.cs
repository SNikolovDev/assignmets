﻿using System;
using System.Collections.Generic;
using LINQify.Tasks;

namespace LINQify
{
    class Program
    {
        static void Main(string[] args)
        {
            var people = Helper.GetData();

            //You can test your implementations here:

           var result1 = Task12.Execute(people);
           var result2 = Task12.ExecuteWithLINQ(people);

            Console.WriteLine(result1);
            Console.WriteLine(result2);


            foreach (var item in result1)
            {
                Console.WriteLine(item.FirstName + " " + item.LastName);
            }

            Console.WriteLine(new string('-', 20));

            foreach (var item in result2)
            {
                Console.WriteLine(item.FirstName + " " + item.LastName);
            }
        }
    }
}