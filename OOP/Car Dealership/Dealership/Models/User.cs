﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Dealership.Exceptions;
using Dealership.Models.Contracts;

namespace Dealership.Models
{
    public class User : IUser
    {
        private const string UsernamePattern = "^[A-Za-z0-9]+$";
        private const string InvalidUsernameFormatError = "Username contains invalid symbols!";
        private const string InvalidUsernameLengthError = "Username must be between 2 and 20 characters long!";

        private const int NameMinLength = 2;
        private const int NameMaxLength = 20;
        private const string InvalidNameError = "name must be between 2 and 20 characters long!";

        private const int PasswordMinLength = 5;
        private const int PasswordMaxLength = 30;
        private const string PasswordPattern = "^[A-Za-z0-9@*_-]+$";
        private const string InvalidPasswordFormatError = "Username contains invalid symbols!";
        private const string InvalidPasswordLengthError = "Password must be between 5 and 30 characters long!";

        private const int MaxVehiclesToAdd = 5;

        private const string NotAnVipUserVehiclesAdd = "You are not VIP and cannot add more than {0} vehicles!";
        private const string AdminCannotAddVehicles = "You are an admin and therefore cannot add vehicles!";
        private const string YouAreNotTheAuthor = "You are not the author of the comment you are trying to remove!";
        private const string NoVehiclesHeader = "--NO VEHICLES--";

        private string username;
        private string firstName;
        private string lastName;
        private string password;
        private IList<IVehicle> vehicles = new List<IVehicle>();
        private IList<IComment> comments = new List<IComment>();

        public User(string username, string firstName, string lastName, string password, Role role)
        {
            this.Username = username;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Password = password;
            this.Role = role;
        }

        public string Username
        {
            get => username;
            set
            {
                Validator.ValidateStringLenght(value, NameMinLength, NameMaxLength, InvalidUsernameLengthError);
                Validator.ValidateSymbols(value, UsernamePattern, InvalidUsernameFormatError);

                this.username = value;
            }
        }

        public string FirstName
        {
            get => firstName;
            set
            {
                Validator.ValidateStringLenght(value, NameMinLength, NameMaxLength, InvalidNameError);

                this.firstName = value;
            }
        }

        public string LastName
        {
            get => lastName;
            set
            {
                Validator.ValidateStringLenght(value, NameMinLength, NameMaxLength, InvalidNameError);

                this.lastName = value;
            }
        }
        public string Password
        {
            get => password;
            set
            {
                Validator.ValidateStringLenght(value, PasswordMinLength, PasswordMaxLength, InvalidPasswordLengthError);
                Validator.ValidateSymbols(value, PasswordPattern, InvalidPasswordFormatError);

                this.password = value;
            }
        }

        public Role Role { get; set; }

        public IList<IVehicle> Vehicles => new List<IVehicle>(this.vehicles);

        public IList<IComment> Comments => new List<IComment>(this.comments);

        public void AddComment(IComment commentToAdd, IVehicle vehicleToAddComment)
        {
            vehicleToAddComment.AddComment(commentToAdd);
        }

        public void AddVehicle(IVehicle vehicle)
        {
            if (this.Role == Role.Admin)
            {
                throw new ArgumentException(AdminCannotAddVehicles);
            }
            else if (this.Role == Role.Normal && this.vehicles.Count >= 5)
            {
                string message = string.Format(NotAnVipUserVehiclesAdd, MaxVehiclesToAdd);
                throw new ArgumentException(message);
            }

            this.vehicles.Add(vehicle);
        }

        public string PrintVehicles()
        {
            StringBuilder sb = new StringBuilder();

            if (this.vehicles.Count > 0)
            {
                int counter = 0;

                foreach (var vehicle in this.vehicles)
                {
                    sb.Append($"{++counter}. ");
                    sb.AppendLine(vehicle.VehicleInfo());
                }
            }
            else
            {
                sb.AppendLine(NoVehiclesHeader);
            }

            return sb.ToString().TrimEnd();
        }

        public void RemoveComment(IComment commentToRemove, IVehicle vehicleToRemoveComment)
        {
            if (this.Username != commentToRemove.Author)
            {
                throw new ArgumentException(YouAreNotTheAuthor);
            }

            vehicleToRemoveComment.RemoveComment(commentToRemove);
        }

        public void RemoveVehicle(IVehicle vehicle)
        {
            this.vehicles.Remove(vehicle);
        }

        public override string ToString()
        {
            return $"Username: {this.Username}, FullName: {this.FirstName} {this.LastName}, Role: {this.Role}";
        }
    }
}
