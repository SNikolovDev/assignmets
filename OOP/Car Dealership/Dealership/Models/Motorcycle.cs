﻿using System.Text;

using Dealership.Models.Contracts;

namespace Dealership.Models
{
    public class Motorcycle : Vehicle, IMotorcycle
    {
        public const int CategoryMinLength = 3;
        public const int CategoryMaxLength = 10;
        public const string InvalidCategoryError = "Category must be between 3 and 10 characters long!";

        private const int wheels = 2;

        private string category;

        public Motorcycle(string make, string model, decimal price, string category)
            : base(make, model, price)
        {
            this.Category = category;
        }

        public string Category
        {
            get => this.category;
            set
            {
                Validator.ValidateStringLenght(value, CategoryMinLength, CategoryMaxLength, InvalidCategoryError);

                this.category = value;
            }
        }

        public override VehicleType Type => VehicleType.Motorcycle;

        public override int Wheels => wheels;

        public override string AdditionalInfo => $"Category: {this.Category}";

        public override string VehicleInfo()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(base.VehicleInfo());

            return sb.ToString().TrimEnd();
        }
    }
}
