﻿using System.Text;

using Dealership.Models.Contracts;

namespace Dealership.Models
{
    public class Truck : Vehicle, ITruck
    {
        public const int MinCapacity = 1;
        public const int MaxCapacity = 100;
        public const string InvalidCapacityError = "Weight capacity must be between 1 and 100!";

        private int weightCapacity;

        public Truck(string make, string model, decimal price, int weightCapacity)
            : base(make, model, price)
        {
            this.WeightCapacity = weightCapacity;
        }

        public int Seats => throw new System.NotImplementedException();

        public override VehicleType Type => throw new System.NotImplementedException();

        public override int Wheels => throw new System.NotImplementedException();

        public int WeightCapacity
        {
            get => this.weightCapacity;
            set
            {
                Validator.ValidateIntRange(value, MinCapacity, MaxCapacity, InvalidCapacityError);

                this.weightCapacity = value;
            }
        }

        public override string AdditionalInfo => $"Weight capacity: {this.WeightCapacity}t";

        public override string VehicleInfo()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(base.VehicleInfo());

            return sb.ToString().TrimEnd();
        }
    }
}
