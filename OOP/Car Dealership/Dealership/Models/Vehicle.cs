﻿using System;
using System.Collections.Generic;
using System.Text;

using Dealership.Models.Contracts;

namespace Dealership.Models
{
    public abstract class Vehicle : IVehicle
    {
        public const int MakeMinLength = 2;
        public const int MakeMaxLength = 15;
        public const string InvalidMakeError = "Make must be between 2 and 15 characters long!";
        public const int ModelMinLength = 1;
        public const int ModelMaxLength = 15;
        public const string InvalidModelError = "Model must be between 1 and 15 characters long!";
        public const decimal MinPrice = 0.0m;
        public const decimal MaxPrice = 1000000.0m;
        public const string InvalidPriceError = "Price must be between 0.0 and 1000000.0!";

        private string make;
        private string model;
        private decimal price;

        private IList<IComment> comments = new List<IComment>();

        protected Vehicle(string make, string model, decimal price)
        {
            this.Make = make;
            this.Model = model;
            this.Price = price;
        }

        public string Make
        {
            get => this.make;
            set
            {
                Validator.ValidateStringLenght(value, MakeMinLength, MakeMaxLength, InvalidMakeError);

                this.make = value;
            }
        }

        public string Model
        {
            get => this.model;
            set
            {
                Validator.ValidateStringLenght(value, ModelMinLength, ModelMaxLength, InvalidModelError);

                this.model = value;
            }
        }

        public abstract VehicleType Type { get; }

        public abstract int Wheels { get; }

        public decimal Price
        {
            get => this.price;
            set
            {
                Validator.ValidateDecimalRange(value, MinPrice, MaxPrice, InvalidPriceError);

                this.price = value;
            }
        }

        public IList<IComment> Comments => new List<IComment>(this.comments);

        public abstract string AdditionalInfo { get; }

        public void AddComment(IComment comment)
        {
            if (comment is null)
            {
                throw new ArgumentNullException(nameof(comment));
            }

            this.comments.Add(comment);
        }

        public void RemoveComment(IComment comment)
        {
            if (comment is null)
            {
                throw new ArgumentNullException(nameof(comment));
            }

            this.comments.Remove(comment);
        }

        public virtual string VehicleInfo()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine($" {this.Type}");
            sb.AppendLine($"Make: {this.Make}");
            sb.AppendLine($"Model: {this.Model}");
            sb.AppendLine($"Wheels: {this.Wheels}");
            sb.AppendLine($"Price: ${this.Price}");
            sb.AppendLine(this.AdditionalInfo);

            if (this.comments.Count > 0)
            {
                sb.AppendLine($"  --COMMENTS--");

                foreach (var comment in this.comments)
                {
                    sb.AppendLine($"  {comment.Content}");
                    sb.AppendLine($"    User: {comment.Author}");
                }
            }
            else
            {
                sb.AppendLine("  --NO COMMENTS--");
            }

            return sb.ToString().TrimEnd();
        }
    }
}
