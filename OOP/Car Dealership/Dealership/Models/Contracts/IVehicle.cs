﻿using System.Collections.Generic;

namespace Dealership.Models.Contracts
{
    public interface IVehicle : IPriceable
    {
        string Make { get; }

        string Model { get; }

        VehicleType Type { get; }

        int Wheels { get; }

        IList<IComment> Comments { get; }

        void AddComment(IComment comment);

        void RemoveComment(IComment comment);

        string VehicleInfo();
    }
}
