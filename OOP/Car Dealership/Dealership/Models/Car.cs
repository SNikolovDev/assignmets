﻿using System.Text;

using Dealership.Models.Contracts;

namespace Dealership.Models
{
    public class Car : Vehicle, ICar
    {
        public const int MinSeats = 1;
        public const int MaxSeats = 10;
        public const string InvalidSeatsError = "Seats must be between 1 and 10!";

        private int seats;
        private const int wheels = 4;

        public Car(string make, string model, decimal price, int seats)
            : base(make, model, price)
        {
            this.Seats = seats;
        }

        public override VehicleType Type => VehicleType.Car;

        public int Seats
        {
            get => this.seats;
            set
            {
                Validator.ValidateIntRange(value, MinSeats, MaxSeats, InvalidSeatsError);

                this.seats = value;
            }
        }
        public override int Wheels => wheels;

        public override string AdditionalInfo => $"Seats: {this.Seats}";

        public override string VehicleInfo()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(base.VehicleInfo());
            sb.AppendLine(this.AdditionalInfo);

            return sb.ToString().TrimEnd();
        }
    }
}
