﻿
using System.Collections.Generic;
using System.Text;

using Dealership.Core.Contracts;
using Dealership.Exceptions;

namespace Dealership.Commands
{
    public class ShowUsersCommand : BaseCommand
    {
        public ShowUsersCommand(List<string> parameters, IRepository repository)
            : base(parameters, repository)
        {
        }

        protected override bool RequireLogin
        {
            get { return true; }
        }

        protected override string ExecuteCommand()
        {
            if (this.CommandParameters.Count < 1)
            {
                throw new InvalidUserInputException($"Invalid number of arguments. Expected: 1, Received: {this.CommandParameters.Count}");
            }

            return ShowUsers();
        }

        private string ShowUsers()
        {
            StringBuilder sb = new StringBuilder();

            int counter = 0;
            foreach (var user in this.Repository.Users)
            {
                sb.AppendLine($"{++counter} {user.ToString()}");
            }

            return sb.ToString().TrimEnd();
        }
    }
}
