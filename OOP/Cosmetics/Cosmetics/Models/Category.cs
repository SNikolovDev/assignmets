﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cosmetics.Models
{
    public class Category
    {
        private string name;
        private List<Product> products = new List<Product>();

        public Category(string name)
        {
            this.Name = name;
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (value.Length < 2 || value.Length > 15)
                {
                    throw new Exception("Invalid category name lenght.");
                }

                this.name = value;
            }
        }

        public List<Product> Products
        {
            get
            {
                return new List<Product>(this.products);
            }
        }

        public void AddProduct(Product product)
        {
            this.products.Add(product);
        }

        public void RemoveProduct(Product product)
        {
            this.products.Remove(product);
        }

        public string Print()
        {
            var productsToPrint = this.products.OrderBy(p => p.Name).ThenByDescending(p => p.Price).ToList();

            StringBuilder builder = new StringBuilder();

            builder.AppendLine($"#Category: {this.Name}");


            if (productsToPrint.Count > 0)
            {
                foreach (var product in productsToPrint)
                {

                    builder.AppendLine(product.Print());
                    builder.AppendLine(" ===");
                }
            }
            else
            {
                builder.AppendLine("No products in this category.");
            }

            return builder.ToString().TrimEnd();
        }
    }
}

