﻿using System;
using System.Text;

namespace Cosmetics.Models
{
    public class Product
    {
        private string name;
        private string brand;
        private double price;
        private readonly GenderType gender;

        public Product(string name, string brand, double price, GenderType gender)
        {
            this.Name = name;
            this.Brand = brand;
            this.Price = price;
            this.gender = gender;
        }
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (value.Length < 3 || value.Length > 10)
                {
                    throw new Exception("Invalid name lenght.");
                }

                this.name = value;
            }
        }

        public string Brand
        {
            get
            {
                return this.brand;
            }
            set
            {
                if (value.Length < 2 || value.Length > 10)
                {
                    throw new Exception("Invalid brand name lenght.");
                }

                this.brand = value;
            }
        }

        public double Price
        {
            get
            {
                return this.price;
            }
            set
            {
                if (value < 0)
                {
                    throw new Exception("Price can't be negative.");
                }

                this.price = value;
            }
        }

        public GenderType Gender
        {
            get
            {
                return this.gender;
            }
        }

        public string Print()
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine($" #{this.Name} {this.Brand}");
            builder.AppendLine($" #Price: {this.Price}");
            builder.AppendLine($" #Gender {this.Gender}");

            return builder.ToString().TrimEnd();
        }

        public override bool Equals(object p)
        {
            if (p == null || !(p is Product))
            {
                return false;
            }

            if (this == p)
            {
                return true;
            }

            Product otherProduct = (Product)p;

            return this.Price == otherProduct.Price
                    && this.Name == otherProduct.Name
                    && this.Brand == otherProduct.Brand
                    && this.Gender == otherProduct.Gender;
        }
    }
}