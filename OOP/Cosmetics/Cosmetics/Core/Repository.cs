﻿using Cosmetics.Core.Contracts;
using Cosmetics.Models;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Cosmetics.Core
{
    public class Repository : IRepository
    {
        private readonly List<Product> products;
        private readonly List<Category> categories;
        private readonly ShoppingCart shoppingCart;

        public Repository()
        {
            this.products = new List<Product>();
            this.categories = new List<Category>();

            this.shoppingCart = new ShoppingCart();
        }

        public ShoppingCart ShoppingCart
        {
            get
            {
                return this.shoppingCart;
            }
        }

        public List<Category> Categories
        {
            get
            {
                return new List<Category>(this.categories);
            }
        }

        public List<Product> Products
        {
            get
            {
                return new List<Product>(this.products);
            }
        }

        public Product FindProductByName(string productName)
        {
            var result = products.FirstOrDefault(p => p.Name == productName);
            return result == null ? throw new ArgumentException($"Product {productName} does not exist") : result;
        }

        public Category FindCategoryByName(string categoryName)
        {
            var result = categories.FirstOrDefault(c => c.Name == categoryName);
            return result == null ? throw new ArgumentException($"Category {categoryName} does not exist") : result;
        }

        public void CreateCategory(string categoryName)
        {
            this.categories.Add(new Category(categoryName));
        }

        public void CreateProduct(string name, string brand, double price, GenderType gender)
        {
            this.products.Add(new Product(name, brand, price, gender));
        }

        public bool CategoryExist(string categoryName)
        {
            return categories.Any(c => c.Name == categoryName);
        }

        public bool ProductExist(string productName)
        {
            return products.Any(p => p.Name == productName);
        }
    }
}
