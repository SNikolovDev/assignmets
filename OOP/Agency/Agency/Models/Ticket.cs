﻿using System;

using Agency.Exceptions;
using Agency.Models.Contracts;

namespace Agency.Models
{
    public class Ticket : ITicket
    {
        private double administrativeCosts;

        public Ticket(int id, IJourney journey, double administrativeCosts)
        {
            this.Journey = journey;
            this.AdministrativeCosts = administrativeCosts;
        }

        public int Id { get; set; }

        public IJourney Journey { get; set; }

        public double AdministrativeCosts
        {
            get => administrativeCosts;
            set
            {
                if (value < 0)
                {
                    throw new InvalidUserInputException("Administrative Costs can't be negative number.");
                }

                this.administrativeCosts = value;
            }
        }

        public double CalculatePrice()
        {
            return this.AdministrativeCosts * this.Journey.CalculateTravelCosts();
        }
    }
}
