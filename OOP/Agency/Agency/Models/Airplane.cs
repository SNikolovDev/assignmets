﻿using System.Text;

using Agency.Models.Contracts;

namespace Agency.Models
{
    public class Airplane : Vehicle, IAirplane
    {
        public const int PassengerCapacityMinValue = 1;
        public const int PassengerCapacityMaxValue = 800;
        public const double PricePerKilometerMinValue = 0.10;
        public const double PricePerKilometerMaxValue = 2.50;

        private bool isLowCost;

        public Airplane(int id, int passengerCapacity, double pricePerKilometer, bool isLowCost)
            : base(id, passengerCapacity, pricePerKilometer)
        {
            this.IsLowCost = isLowCost;
        }

        public bool IsLowCost
        {
            get => this.isLowCost;
            set => this.isLowCost = value;
        }

        protected override string VehicleType => "Airplane";

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(base.ToString());
            sb.AppendLine($"Is low-cost: {this.IsLowCost}");

            return sb.ToString().TrimEnd();
        }
    }
}
