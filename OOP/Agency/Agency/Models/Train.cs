﻿using System.Text;

using Agency.Exceptions;
using Agency.Models.Contracts;

namespace Agency.Models
{
    public class Train : Vehicle, ITrain
    {
        public const int PassengerCapacityMinValue = 30;
        public const int PassengerCapacityMaxValue = 150;
        public const double PricePerKilometerMinValue = 0.10;
        public const double PricePerKilometerMaxValue = 2.50;
        public const int CartsMinValue = 1;
        public const int CartsMaxValue = 15;

        private int passengerCapacity;
        private double pricePerKilometer;
        private int carts;

        public Train(int id, int passengerCapacity, double pricePerKilometer, int carts)
            : base(id, passengerCapacity, pricePerKilometer)
        {
            this.PassengerCapacity = passengerCapacity;
            this.PricePerKilometer = pricePerKilometer;
            this.Carts = carts;
        }

        public override int PassengerCapacity
        {
            get => passengerCapacity;
            set
            {
                if (value < PassengerCapacityMinValue || value > PassengerCapacityMaxValue)
                {
                    string message = string.Format(ExceptionMessages.InvalidCapacity, this.GetType().Name.ToLower(), PassengerCapacityMinValue, PassengerCapacityMaxValue);
                    throw new InvalidUserInputException(message);
                }

                this.passengerCapacity = value;
            }
        }

        public override double PricePerKilometer
        {
            get => pricePerKilometer;
            set
            {
                if (value < PricePerKilometerMinValue || value > PricePerKilometerMaxValue)
                {
                    throw new InvalidUserInputException("Invalid price per kilometer");
                }

                this.pricePerKilometer = value;
            }
        }

        public int Carts
        {
            get => carts;
            set
            {
                if (value < CartsMinValue || value > CartsMaxValue)
                {
                    string message = string.Format(ExceptionMessages.InvalidTrainCartsCount, CartsMinValue, CartsMaxValue);
                    throw new InvalidUserInputException(message);
                }

                this.carts = value;
            }
        }

        protected override string VehicleType => "Train";

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(base.ToString());
            sb.AppendLine($"Carts amount: {this.Carts}");

            return sb.ToString().TrimEnd();
        }
    }
}
