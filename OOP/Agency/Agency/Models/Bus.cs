﻿using System;
using System.Text;

using Agency.Exceptions;
using Agency.Models.Contracts;

namespace Agency.Models
{
    public class Bus : Vehicle, IBus
    {
        public const int PassengerCapacityMinValue = 10;
        public const int PassengerCapacityMaxValue = 50;
        public const double PricePerKilometerMinValue = 0.10;
        public const double PricePerKilometerMaxValue = 2.50;

        private int passengerCapacity;
        private double pricePerKilometer;

        public Bus(int id, int passengerCapacity, double pricePerKilometer, bool hasFreeTv)
            : base(id, passengerCapacity, pricePerKilometer)
        {
            this.PassengerCapacity = passengerCapacity;
            this.PricePerKilometer = pricePerKilometer;
            this.HasFreeTv = hasFreeTv;
        }

        public override int PassengerCapacity
        {
            get => passengerCapacity;
            set
            {
                if (value < PassengerCapacityMinValue || value > PassengerCapacityMaxValue)
                {
                    string message = string.Format(ExceptionMessages.InvalidCapacity, this.GetType().Name.ToLower(), PassengerCapacityMinValue, PassengerCapacityMaxValue);
                    throw new InvalidUserInputException(message);
                }

                this.passengerCapacity = value;
            }
        }

        public override double PricePerKilometer
        {
            get => pricePerKilometer;
            set
            {
                if (value < PricePerKilometerMinValue || value > PricePerKilometerMaxValue)
                {
                    throw new InvalidUserInputException("Wrong price.");
                }

                this.pricePerKilometer = value;
            }
        }

        public bool HasFreeTv { get; set; }

        protected override string VehicleType => "Bus";

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(base.ToString());
            sb.AppendLine($"Has free TV: {this.HasFreeTv}");

            return sb.ToString().TrimEnd();
        }
    }
}
