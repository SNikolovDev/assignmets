﻿using System;

using Agency.Exceptions;
using Agency.Models.Contracts;

namespace Agency.Models
{
    public class Journey : IJourney
    {
        public const int StartLocationMinLength = 5;
        public const int StartLocationMaxLength = 25;
        public const int DestinationMinLength = 5;
        public const int DestinationMaxLength = 25;
        public const int DistanceMinValue = 5;
        public const int DistanceMaxValue = 5000;

        private string from;
        private string to;
        private int distance;
        private IVehicle vehicle;

        public Journey(int id, string from, string to, int distance, IVehicle vehicle)
        {
            this.Id = id;
            this.From = from;
            this.To = to;
            this.Distance = distance;
            this.Vehicle = vehicle;
        }

        public int Id { get; set; }

        public string From
        {
            get => from;
            set
            {
                if (value.Length < StartLocationMinLength || value.Length > StartLocationMaxLength)
                {
                    string message = string.Format(ExceptionMessages.InvalidStringLenght, nameof(From), StartLocationMinLength, StartLocationMaxLength);
                    throw new InvalidUserInputException(message);
                }

                this.from = value;
            }
        }

        public string To
        {
            get => to;
            set
            {
                if (value.Length < DestinationMinLength || value.Length > DestinationMaxLength)
                {
                    string message = string.Format(ExceptionMessages.InvalidStringLenght, nameof(To), DestinationMinLength, DestinationMaxLength);
                    throw new InvalidUserInputException(message);
                }

                this.to = value;
            }
        }

        public int Distance
        {
            get => distance;
            set
            {
                if (value < DistanceMinValue || value > DistanceMaxValue)
                {
                    string message = string.Format(ExceptionMessages.InvalidDistanceLenght, nameof(To), DistanceMinValue, DistanceMaxValue);
                    throw new InvalidUserInputException(message);
                }

                this.distance = value;
            }
        }

        public IVehicle Vehicle
        {
            get
            {
                return this.vehicle;
            }
            set
            {
                this.vehicle = value;
            }
        }
        
        public double CalculateTravelCosts()
        {
            return this.Distance * this.Vehicle.PricePerKilometer;
        }
    }
}
