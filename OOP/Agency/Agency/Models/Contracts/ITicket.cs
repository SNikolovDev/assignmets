﻿namespace Agency.Models.Contracts
{
    public interface ITicket : IIdentifiable
    {
        double AdministrativeCosts { get; }
        IJourney Journey { get; }
        double CalculatePrice();
    }
}