﻿namespace Agency.Models.Contracts
{
    public interface ITrain : IVehicle, IIdentifiable
    {
        int Carts { get; }
    }
}