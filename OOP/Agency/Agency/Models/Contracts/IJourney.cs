﻿namespace Agency.Models.Contracts
{
    public interface IJourney : IIdentifiable
    {
        string From { get; }
        string To { get; }
        int Distance { get; }
        IVehicle Vehicle { get; }
        double CalculateTravelCosts();
    }
}