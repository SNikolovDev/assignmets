﻿namespace Agency.Models.Contracts
{
    public interface IBus : IVehicle, IIdentifiable
    {
        bool HasFreeTv { get; }
    }
}