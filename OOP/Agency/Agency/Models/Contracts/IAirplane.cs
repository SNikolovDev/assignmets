﻿namespace Agency.Models.Contracts
{
    public interface IAirplane :IVehicle,  IIdentifiable
    {
        bool IsLowCost { get; }
    }
}