﻿namespace Agency.Models.Contracts
{
    public interface IIdentifiable
    {
        int Id { get; }
    }
}
