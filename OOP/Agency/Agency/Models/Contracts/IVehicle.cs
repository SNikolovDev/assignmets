﻿namespace Agency.Models.Contracts
{
    public interface IVehicle : IIdentifiable
    {
        int PassengerCapacity { get; set; }

        double PricePerKilometer { get; set; }
    }
}