﻿using System.Text;

using Agency.Exceptions;
using Agency.Models.Contracts;

namespace Agency.Models
{
    public abstract class Vehicle : IVehicle
    {
        private const int PassengerCapacityMinValue = 1;
        private const int PassengerCapacityMaxValue = 800;
        private const double PricePerKilometerMinValue = 0.10;
        private const double PricePerKilometerMaxValue = 2.50;

        private int passengerCapacity;
        private double pricePerKilometer;
        private static int hasId = 0;

        public Vehicle(int id, int passengerCapacity, double pricePerKilometer)
        {
            hasId++;
            this.PassengerCapacity = passengerCapacity;
            this.PricePerKilometer = pricePerKilometer;
        }

        public int Id => hasId;

        public virtual int PassengerCapacity
        {
            get => this.passengerCapacity;
            set
            {
                if (value < PassengerCapacityMinValue || value > PassengerCapacityMaxValue)
                {
                    string message = string.Format(ExceptionMessages.InvalidVehiclePassangersCapacity, this.GetType().Name.ToLower(), PassengerCapacityMinValue, PassengerCapacityMaxValue);
                    throw new InvalidUserInputException(message);
                }

                this.passengerCapacity = value;
            }
        }

        public virtual double PricePerKilometer
        {
            get => this.pricePerKilometer;
            set
            {
                if (value < PricePerKilometerMinValue || value > PricePerKilometerMaxValue)
                {
                    string message = string.Format(ExceptionMessages.InvalidVehiclePricePerKm, this.GetType().Name.ToLower(), PricePerKilometerMinValue, PricePerKilometerMaxValue);
                    throw new InvalidUserInputException(message);
                }

                this.pricePerKilometer = value;
            }
        }

        protected abstract string VehicleType { get; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"{this.VehicleType} ----");
            sb.AppendLine($"Passenger capacity: {this.PassengerCapacity}");
            sb.AppendLine($"Price per kilometer capacity: {this.PricePerKilometer}");

            return sb.ToString().TrimEnd();
        }
    }
}
