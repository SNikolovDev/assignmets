﻿namespace Agency.Exceptions
{
    internal class ExceptionMessages
    {
        public const string InvalidVehiclePassangersCapacity = "A vehicle with less than {0} passengers or more than {1} passengers cannot exist!";
        public const string InvalidVehiclePricePerKm = "A vehicle with a price per kilometer lower than ${0} or higher than ${1} cannot exist!";
        public const string InvalidCapacity = "A {0} cannot have less than {1} passengers or more than {2} passengers.";
        public const string InvalidPricePerKm = "Ticket price for {0} can't be less than [1} and more than {2}.";
        public const string InvalidTrainCartsCount = "A train cannot have less than {1} cart or more than {1} carts.";
        public const string InvalidStringLenght = "The {0}'s length cannot be less than {1} or more than {2} symbols long.";
        public const string InvalidDistanceLenght = " The {0} cannot be less than {1} or more than {2} kilometers.";
    }
}
