﻿using Agency.Commands.Abstracts;
using Agency.Core.Contracts;
using Agency.Exceptions;

using System;
using System.Collections.Generic;

namespace Agency.Commands
{
    public class CreateAirplaneCommand : BaseCommand
    {
        private const int ExpectedParametersCount = 3;

        public CreateAirplaneCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            //TODO Implement command.
            //[passengerCapacity]
            //[pricePerKilometer]
            //[isLowCost]
            if (this.CommandParameters.Count < ExpectedParametersCount)
            {
                throw new InvalidUserInputException($"Invalid number of arguments. Expected: {ExpectedParametersCount}, Received: {this.CommandParameters.Count}");
            }

            int passangerCapacity = this.ParseIntParameter(this.CommandParameters[0], "passengerCapacity");
            double pricePerKilometer = this.ParseDoubleParameter(this.CommandParameters[1], "pricePerKilometer");
            bool isLowCost = this.ParseBoolParameter(this.CommandParameters[2], "isLowCost");

            var airplain = this.Repository.CreateAirplane(passangerCapacity, pricePerKilometer, isLowCost);
            return $"Airplain with ID {airplain.Id} was created.";
        }
    }
}
