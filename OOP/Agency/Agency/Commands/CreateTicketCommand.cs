﻿using Agency.Commands.Abstracts;
using Agency.Core.Contracts;
using Agency.Exceptions;
using Agency.Models;

using System.Collections.Generic;

namespace Agency.Commands
{
    public class CreateTicketCommand : BaseCommand
    {
        private const int ExpectedParametersCount = 2;
        public CreateTicketCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count < ExpectedParametersCount)
            {
                throw new InvalidUserInputException($"Invalid number of arguments. Expected: {ExpectedParametersCount}, Received: {this.CommandParameters.Count}");
            }

            // [journeyID]
            // [administrativeCosts]
            var jorneyId = this.ParseIntParameter(this.CommandParameters[0], "jorneyID");
            var journey = this.Repository.FindJourneyById(jorneyId);
            var administrativeCosts = this.ParseDoubleParameter(this.CommandParameters[1], "administrativeCosts");

            var ticket = this.Repository.CreateTicket(journey, administrativeCosts);
            return $"Ticket with ID {ticket.Id} was created.";
        }
    }
}
