﻿namespace Library.Models
{
	public class Book
	{
		public int Id { get; set; }

		public string Title { get; set; }

		public string Author { get; set; }

		public string Genre { get; set; }// TODO: Make this enum;

		public int PublishedYear { get; set; }

		public string Description { get; set; } // not mandatory;

        public int UserId { get; set; }

        // TODO: add photo property here
    }
}
