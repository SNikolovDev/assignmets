﻿using System.ComponentModel.DataAnnotations;

using Library.Helpers;

namespace Library.Models.DTOs
{
	public class CreateBook
	{
		[Required, DataType("NVARCHAR")]
		public string Title { get; set; }

		[Required, DataType("NVARCHAR")]	
		public string Author { get; set; }

		[DataType("NVARCHAR")]
		public string Genre { get; set; }// TODO: Make this enum;

		[Range(1800, 2100, ErrorMessage = Constants.InvalidYearErrorMessage)]
		public int PublishedYear { get; set; }

		[DataType("NVARCHAR")]
		public string Description { get; set; } // not mandatory;

		[Required]
		public int UserId { get; set; }
	}
}
