﻿using System.ComponentModel.DataAnnotations;

using Library.Helpers;

namespace Library.Models.DTOs
{
	public class CreateUser
	{
		[Required(ErrorMessage = Constants.RequiredUsernameErrorMessage)]
		[StringLength(32, MinimumLength = 4)]
		public string Username { get; set; }

		[Required(ErrorMessage = Constants.RequiredPassowrdErrorMessage)]
		[StringLength(32, MinimumLength = 4)]
		public string Password { get; set; }

		[EmailAddress(ErrorMessage = Constants.InvalidEmailErrorMessage)]
		public string Email { get; set; }
	}
}
