﻿namespace Library.Helpers
{
	public static class Constants
	{
		public const string InvalidYearErrorMessage = "Please put valid year!";
		
		public const string InvalidEmailErrorMessage = "This is not a valid email address!";

		public const string RequiredPassowrdErrorMessage = "Password is required";

		public const string RequiredUsernameErrorMessage = "Username is required";
	}
}
