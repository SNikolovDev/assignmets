﻿using Library.Models;

using Microsoft.EntityFrameworkCore;

namespace Library.Data
{
	public class ApplicationContext : DbContext
	{
		public ApplicationContext(DbContextOptions<ApplicationContext> options) 
			: base(options)
		{
		}

		public DbSet<Book> Books { get; set; }

		public DbSet<User> Users { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder); // TODO: Add initial seed for testing purposes;
		}
	}
}
