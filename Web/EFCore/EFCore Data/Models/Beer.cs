﻿using System.ComponentModel.DataAnnotations;

namespace EFCore_Data.Models
{
    public class Beer
    {
        [Key]
        public int BeerId { get; set; }

        [Required, MinLength(3), MaxLength(50)]
        public string Name { get; set; }

        public double ABV { get; set; }

        public int BreweryId { get; set; } // Foreign Key
        public Brewery Brewery { get; set; }
    }
}