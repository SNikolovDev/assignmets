﻿using EFCore_Data.Models;

using Microsoft.EntityFrameworkCore;

namespace EFCore_Data
{
    public class BeerContext : DbContext
    {
        public BeerContext(DbContextOptions<BeerContext> options)
            : base(options)
        {
            
        }

        public DbSet<Beer> Beers { get; set; }

        public DbSet<Brewery> Breweries { get; set; }
    }
}
