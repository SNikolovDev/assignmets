﻿using AspNetCore.Models;
using AspNetCore.Service;

using Microsoft.AspNetCore.Mvc;

namespace AspNetCore.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BeersController : ControllerBase
    {
        private readonly IBeerService beerService;

        public BeersController(IBeerService beerService)
        {
            this.beerService = beerService;
        }

        [HttpGet("")]
        public IActionResult GetAll()
        {
            return Ok(this.beerService.Get());
        }

        [HttpGet("")]
        public IActionResult Get([FromQuery] BeerQueryParameters filterParameters)
        {
            var result = new List<Beer>();

            if (!string.IsNullOrEmpty(filterParameters.Name))
            {
                result = result.FindAll(b => b.Name.Contains(filterParameters.Name, StringComparison.InvariantCultureIgnoreCase));
            }

            if (filterParameters.MinAbv.HasValue)
            {
                result = result.FindAll(b => b.Abv >= filterParameters.MinAbv);
            }

            if (filterParameters.MaxAbv.HasValue)
            {
                result = result.FindAll(b => b.Abv <= filterParameters.MaxAbv);
            }

            if (!string.IsNullOrEmpty(filterParameters.SortBy))
            {
                if (filterParameters.SortBy.Equals("name", StringComparison.InvariantCultureIgnoreCase))
                {
                    result = result.OrderBy(b => b.Name).ToList();
                }
                else if (filterParameters.SortBy.Equals("abv", StringComparison.InvariantCultureIgnoreCase))
                {
                    result = result.OrderBy(b => b.Abv).ToList();
                }

                if (!string.IsNullOrEmpty(filterParameters.SortOrder) && filterParameters.SortOrder.Equals("desc", StringComparison.InvariantCultureIgnoreCase))
                {
                    result.Reverse();
                }
            }

            return this.StatusCode(StatusCodes.Status200OK, result);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var beer = this.beerService.Get(id);

            if (beer == null)
            {
                return this.StatusCode(StatusCodes.Status404NotFound, $"Beer with id {id} doesn't exist.");
            }

            return this.StatusCode(StatusCodes.Status200OK, beer);
        }

        [HttpGet("{name}")]
        public IActionResult Get(string name)
        {
            var beer = this.beerService.Get(name);

            if (beer == null)
            {
                return this.StatusCode(StatusCodes.Status404NotFound, $"Beer with name {name} doesn't exist.");
            }

            return this.StatusCode(StatusCodes.Status200OK, beer);
        }


        [HttpPost("")]
        public IActionResult Create([FromBody] Beer model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            var beer = this.beerService.Create(model);


            return this.StatusCode(StatusCodes.Status201Created, beer);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Beer model)
        {
            var beerToUpdate = this.beerService.Update(id, model);

            if (beerToUpdate == null)
            {
                return this.StatusCode(StatusCodes.Status404NotFound, $"Beer with id {id} doesn't exist.");
            }

            return this.StatusCode(StatusCodes.Status200OK, beerToUpdate);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var beerToDelete = this.beerService.Get(id);
            this.beerService.Delete(id);

            if (beerToDelete == null)
            {
                return this.StatusCode(StatusCodes.Status404NotFound, $"Beer with id {id} doesn't exist.");
            }

            return this.StatusCode(StatusCodes.Status200OK, beerToDelete);
        }
    }
}
