﻿using AspNetCore.Web.Services;

using AspNetCoreDemo.Exceptions;

using Microsoft.AspNetCore.Mvc;

namespace AspNetCore.Web.Controllers
{
    [ApiController]
    [Route("api/controller")]
    public class StylesController : ControllerBase
    {
        private readonly IStylesService service;

        public StylesController(IStylesService service)
        {
            this.service = service;
        }

        [HttpGet("")]
        public IActionResult Get()
        {
            var styles = this.service.Get();
            return this.StatusCode(StatusCodes.Status200OK, styles);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var style = this.service.Get(id);
                return this.StatusCode(StatusCodes.Status200OK, style);
            }
            catch (EntityNotFoundException e)
            {
                return this.StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
        }
    }
}
