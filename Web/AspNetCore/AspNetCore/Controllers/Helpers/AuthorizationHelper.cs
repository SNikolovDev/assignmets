﻿using AspNetCoreDemo.Exceptions;
using AspNetCoreDemo.Models;
using AspNetCoreDemo.Services;

namespace AspNetCoreDemo.Controllers.Helpers
{
	public class AuthorizationHelper
	{
		private readonly IUsersService usersService;

		public AuthorizationHelper(IUsersService usersService)
		{
			this.usersService = usersService;
		}

        public User TryGetUser(string username)
        {
            try
            {
                return this.usersService.Get(username);
            }
            catch (EntityNotFoundException)
            {
                throw new UnauthorizedOperationException("Invalid Username");
            }
        }
    }
}
