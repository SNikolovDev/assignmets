﻿namespace AspNetCore.Web.Models
{
    public class User
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public bool IsAdmin { get; set; }

        public override bool Equals(object? obj)
        {
            if (obj is User other)
            {
                return this.Username == other.Username;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return this.Username.GetHashCode();
        }
    }
}
