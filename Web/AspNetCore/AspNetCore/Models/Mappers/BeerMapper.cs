﻿using AspNetCore.Models;
using AspNetCore.Web.Services;

using AspNetCoreDemo.Models;

namespace AspNetCore.Web.Models.Mappers
{
    public class BeerMapper
    {
        private readonly IStylesService stylesService;

        public BeerMapper(IStylesService stylesService)
        {
            this.stylesService = stylesService;
        }

        public Beer ConvertToModel(BeerDto dto)
        {
            Beer model = new Beer();
            model.Abv = dto.Abv;
            model.Name = dto.Name;
            model.Style = this.stylesService.Get(dto.StyleId);
            return model;
        }
    }
}
