﻿using System.ComponentModel.DataAnnotations;

using AspNetCore.Web.Models;

namespace AspNetCore.Models
{
    public class Beer
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public double Abv { get; set; }

        public Style Style { get; set; }
    }
}
