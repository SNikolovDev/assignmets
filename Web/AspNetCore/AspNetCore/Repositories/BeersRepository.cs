﻿using AspNetCore.Models;

using AspNetCoreDemo.Exceptions;

namespace AspNetCore.Web.Repositories
{
    public class BeersRepository : IBeersRepository
    {
        private readonly List<Beer> beers;

        public BeersRepository()
        {
            this.beers = new List<Beer>();
            this.beers.Add(new Beer
            {
                Id = 1,
                Name = "Glarus English Ale",
                Abv = 4.6
            });
            this.beers.Add(new Beer
            {
                Id = 2,
                Name = "Rhombus Porter",
                Abv = 5.0
            });
            this.beers.Add(new Beer
            {
                Id = 3,
                Name = "Opasen Char",
                Abv = 6.6
            });
        }

        public Beer Create(Beer beer)
        {
            if (this.beers.Contains(beer))
            {
                throw new DuplicateEntityException();
            }

            this.beers.Add(beer);
            return beer;
        }

        public List<Beer> Get()
        {
            return this.beers;
        }

        public Beer Get(int id)
        {
            var beer = this.beers.FirstOrDefault(b => b.Id == id);
            return beer ?? throw new EntityNotFoundException();
        }

        public Beer Get(string name)
        {
            var beer = this.beers.FirstOrDefault(b => b.Name == name);
            return beer ?? throw new EntityNotFoundException();
        }

        public List<Beer> Get(BeerQueryParameters filterParameters)
        {
            IEnumerable<Beer> result = this.beers;

            if (!string.IsNullOrEmpty(filterParameters.Name))
            {
                result = result.Where(beer => beer.Name.Contains(filterParameters.Name, StringComparison.InvariantCultureIgnoreCase));
            }

            if (filterParameters.MinAbv.HasValue)
            {
                result = result.Where(b => b.Abv >= filterParameters.MinAbv);
            }

            if (filterParameters.MaxAbv.HasValue)
            {
                result = result.Where(b => b.Abv <= filterParameters.MaxAbv);
            }

            if (!string.IsNullOrEmpty(filterParameters.SortBy))
            {
                if (filterParameters.SortBy.Equals("name", StringComparison.InvariantCultureIgnoreCase))
                {
                    result = result.OrderBy(b => b.Name);
                }
                else if (filterParameters.SortBy.Equals("abv", StringComparison.InvariantCultureIgnoreCase))
                {
                    result = result.OrderBy(b => b.Abv);
                }

                if (!string.IsNullOrEmpty(filterParameters.SortOrder) && filterParameters.SortOrder.Equals("desc", StringComparison.InvariantCultureIgnoreCase))
                {
                    result = result.Reverse();
                }
            }

            if (!string.IsNullOrEmpty(filterParameters.SortOrder))
            {
                switch (filterParameters.SortOrder)
                {
                    case "asc":
                        result = result.OrderBy(b => b.Name);
                        break;
                    case "desc":
                        result = result.OrderByDescending(b => b.Name);
                        break;
                    default:
                        break;
                }

                List<Beer> newResult = result.ToList();
                return newResult;
            }

            return result.ToList();
        }

        public Beer Update(int id, Beer beer)
        {
            var beerToUpdate = this.Get(id);
            beerToUpdate.Name = beer.Name;
            beerToUpdate.Abv = beer.Abv;
            return beerToUpdate;

        }

        public Beer Delete(int id)
        {
            var beerToDelete = this.Get(id);
            this.beers.Remove(beerToDelete);
            return beerToDelete;
        }
    }
}
