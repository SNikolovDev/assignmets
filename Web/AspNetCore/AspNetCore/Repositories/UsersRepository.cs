﻿using AspNetCore.Web.Models;

using AspNetCoreDemo.Exceptions;

namespace AspNetCore.Web.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private readonly List<User> users;

        public UsersRepository()
        {
            this.users = new List<User>();
            this.users.Add(new User
            {
                Id = 1,
                Username = "admin",
                IsAdmin = true
            });
            this.users.Add(new User
            {
                Id = 2,
                Username = "gosho",
                IsAdmin = false
            });
            this.users.Add(new User
            {
                Id = 3,
                Username = "pesho",
                IsAdmin = false
            });
        }

        public List<User> Get()
        {
            return this.users;
        }

        public User Get(int id)
        {
            var user = this.users.Where(u => u.Id == id).FirstOrDefault();
            return user ?? throw new EntityNotFoundException();
        }

        public User Get(string username)
        {
            var user = this.users.Where(u => u.Username == username).FirstOrDefault();
            return user ?? throw new EntityNotFoundException();
        }
    }
}
