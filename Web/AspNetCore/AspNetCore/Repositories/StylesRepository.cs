﻿using AspNetCore.Web.Models;

using AspNetCoreDemo.Exceptions;

namespace AspNetCore.Web.Repositories
{
    public class StylesRepository : IStylesRepository
    {
        private readonly List<Style> styles;

        public StylesRepository()
        {
            this.styles = new List<Style>();
            this.styles.Add(new Style
            {
                Id = 1,
                Name = "Special Ale"
            });
            this.styles.Add(new Style
            {
                Id = 2,
                Name = "English Porter"
            });
            this.styles.Add(new Style
            {
                Id = 3,
                Name = "Indian Pale Ale"
            });
        }

        public List<Style> Get()
        {
            return this.styles;
        }

        public Style Get(int id)
        {
            var style = this.styles.Where(s => s.Id == id).FirstOrDefault();

            return style ?? throw new EntityNotFoundException();
        }
    }
}
