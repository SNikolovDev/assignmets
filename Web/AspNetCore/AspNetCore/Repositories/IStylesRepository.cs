﻿using AspNetCore.Web.Models;

namespace AspNetCore.Web.Repositories
{
    public interface IStylesRepository
    {
        List<Style> Get();

        Style Get(int id);
    }
}
