﻿using AspNetCore.Models;

namespace AspNetCore.Web.Repositories
{
    public interface IBeersRepository
    {
        Beer Create(Beer beer);

        List<Beer> Get();

        Beer Get(int id);

        Beer Get(string name);

        List<Beer> Get(BeerQueryParameters filterParameters);

        Beer Update(int id, Beer beer);

        Beer Delete(int id);
    }
}
