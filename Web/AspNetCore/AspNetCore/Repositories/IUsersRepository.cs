﻿using AspNetCore.Web.Models;

namespace AspNetCore.Web.Repositories
{
    public interface IUsersRepository
    {
        List<User> Get();

        User Get(int id);

        User Get(string username);
    }
}
