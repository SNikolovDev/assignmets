﻿using AspNetCore.Web.Models;
using AspNetCore.Web.Repositories;

using AspNetCoreDemo.Services;

namespace AspNetCore.Web.Services
{
    public class UsersService : IUsersService
    {
        private readonly IUsersRepository repository;

        public UsersService(IUsersRepository repository)
        {
            this.repository = repository;
        }

        public List<User> Get()
        {
            return this.repository.Get();
        }

        public User Get(int id)
        {
            return this.repository.Get(id);
        }

        public User Get(string name)
        {
            return this.repository.Get(name);
        }
    }
}
