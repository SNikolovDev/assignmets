﻿using AspNetCore.Web.Models;
using AspNetCore.Web.Repositories;

namespace AspNetCore.Web.Services
{
    public class StylesService : IStylesService
    {
        private readonly IStylesRepository repository;

        public StylesService(IStylesRepository repository)
        {
            this.repository = repository;
        }

        public List<Style> Get()
        {
            return this.repository.Get();
        }

        public Style Get(int id)
        {
            return this.repository.Get(id);
        }
    }
}
