﻿using AspNetCore.Web.Models;

namespace AspNetCoreDemo.Services
{
	public interface IUsersService
	{
        List<User> Get();

        User Get(int id);

        User Get(string name);
    }
}
