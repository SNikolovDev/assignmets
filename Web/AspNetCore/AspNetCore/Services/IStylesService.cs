﻿using AspNetCore.Web.Models;

namespace AspNetCore.Web.Services
{
    public interface IStylesService
    {
        List<Style> Get();

        Style Get(int id);
    }
}
