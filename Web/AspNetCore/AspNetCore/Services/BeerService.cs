﻿using AspNetCore.Models;
using AspNetCore.Web.Repositories;

using AspNetCoreDemo.Exceptions;

namespace AspNetCore.Service
{
    public class BeerService : IBeerService
    {
        private readonly IBeersRepository repository;

        public BeerService(IBeersRepository repository)
        {
            this.repository = repository;
        }

        public Beer Create(Beer beer)
        {
            bool duplicateExists = true;

            try
            {
                this.repository.Get(beer.Name);
            }
            catch (EntityNotFoundException)
            {
                duplicateExists = false;
            }

            if (duplicateExists)
            {
                throw new DuplicateEntityException();
            }

            var createdBeer = this.repository.Create(beer);
            return createdBeer;
        }

        public List<Beer> Get()
        {
            return this.repository.Get();
        }

        public Beer Get(int id)
        {
            return this.repository.Get(id);
        }

        public Beer Get(string name)
        {
            return this.repository.Get(name);
        }

        public List<Beer> Get(BeerQueryParameters filterParameters)
        {
            return this.repository.Get(filterParameters);
        }

        public Beer Update(int id, Beer beer)
        {
            bool duplicateExists = true;
            try
            {
                var existingBeer = this.repository.Get(beer.Name);
                if (existingBeer.Id == beer.Id)
                {
                    duplicateExists = false;
                }
            }
            catch (EntityNotFoundException)
            {
                duplicateExists = false;
            }

            if (duplicateExists)
            {
                throw new DuplicateEntityException();
            }

            var updatedBeer = this.repository.Update(id, beer);
            return updatedBeer;
        }

        public void Delete(int id)
        {
            this.repository.Delete(id);
        }
    }
}
