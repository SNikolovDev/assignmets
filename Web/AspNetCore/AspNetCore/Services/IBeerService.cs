﻿using AspNetCore.Models;

namespace AspNetCore.Service
{
    public interface IBeerService
    {
        List<Beer> Get();

        List<Beer> Get(BeerQueryParameters filterParameters);

        Beer Get(int id);

        Beer Get(string name);

        Beer Create(Beer beer);

        Beer Update(int id, Beer beer);

        void Delete(int id);
    }
}