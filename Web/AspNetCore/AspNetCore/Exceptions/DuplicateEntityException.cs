﻿using System;

namespace AspNetCoreDemo.Exceptions
{
	public class DuplicateEntityException : ApplicationException
	{
        public DuplicateEntityException()
        {
            
        }
    }
}
