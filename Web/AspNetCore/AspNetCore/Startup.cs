using AspNetCore.Service;
using AspNetCore.Web.Repositories;
using AspNetCore.Web.Services;

namespace AspNetCore.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddSingleton<IBeersRepository, BeersRepository>();
            services.AddSingleton<IStylesRepository, StylesRepository>();

            services.AddScoped<IBeerService, BeerService>();
            services.AddScoped<IStylesService, StylesService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
