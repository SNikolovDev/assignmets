﻿using AspNetCore.Models;

namespace AspNetCore.Data
{
    public static class DataBase
    {
        static DataBase()
        {
            Beers = new List<Beer>();
            SeedData();
        }

        public static List<Beer> Beers { get; set; }     

        private static void SeedData()
        {
            Beers.AddRange(new List<Beer>
                {
                new Beer
                {
                    Id = 1,
                    Name = "Glarus English Ale",
                    Abv = 4.6
                },
                new Beer
                {
                    Id = 2,
                    Name = "Rhombus Porter",
                    Abv = 5.0
                }
            });
        }
    };
}
