﻿using System;

using StackQueueWorkshop.Queue;

class Program
{
    static void Main(string[] args)
    {
        var queue = new ArrayQueue<int>();
        queue.Enqueue(1);
        queue.Enqueue(2);
        queue.Enqueue(3);

        queue.Dequeue();
        queue.Dequeue();
        queue.Dequeue();
        queue.Dequeue();
    }
}
