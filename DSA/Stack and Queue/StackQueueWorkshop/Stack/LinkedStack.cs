﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace StackQueueWorkshop.Stack
{
    public class LinkedStack<T> : IStack<T>
    {
        private Node<T> top;
        private Node<T> head;
        private int size;

        public LinkedStack()
        {
            top = new Node<T>();
        }

        public int Size
        {
            get
            {
                return this.size;
            }
        }

        public bool IsEmpty
        {
            get
            {
                return this.size == 0 ? true : false;
            }
        }

        public void Push(T element)
        {
            if (size == 0)
            {
                top.Data = element;
                this.head = top;
                size++;
            }
            else
            {
                top.Next = new Node<T> { Data = element };
                top = top.Next;
                size++;
            }
        }

        public T Pop()
        {
            if (size == 0)
            {
                throw new InvalidOperationException();
            }
            else if (size == 1)
            {
                return this.top.Data;
            }

            var current = this.head;

            while (current.Next.Next != null)
            {
                current = current.Next;
            }

            var nodeToReturn = current.Next;
            current.Next = null;

            return nodeToReturn.Data;
        }

        public T Peek()
        {
            if (this.size == 0)
            {
                throw new InvalidOperationException();
            }

            return this.top.Data;
        }
    }
}
