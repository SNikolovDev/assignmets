﻿using System;
using System.Linq;

namespace StackQueueWorkshop.Stack
{
    public class ArrayStack<T> : IStack<T>
    {
        private T[] items;
        private int top = 0;
        private int arraySize = 4;

        public ArrayStack()
        {
            items = new T[arraySize];
        }

        public int Size
        {
            get
            {
                return top;
            }
        }

        public bool IsEmpty
        {
            get
            {
                if (top == 0)
                {
                    return true;
                }

                return false;
            }
        }

        public void Push(T element)
        {
            if (this.items.Count() - 1 == top)
            {
                this.items = DoubleArraySize(this.items);
            }

            top++;
            this.items[top] = element;
        }

        public T Pop()
        {
            var elementToReturn = this.items[top];
            if (top > 0)
            {
                top--;
            }
            else if (top == 0)
            {
                throw new InvalidOperationException("The stack is empty!");
            }

            return elementToReturn;
        }

        public T Peek()
        {
            if (this.Size > 0)
            {
                return this.items[top];
            }

            throw new InvalidOperationException("The stack is empty!");
        }

        public T[] DoubleArraySize(T[] currentArray)
        {
            var newArray = new T[currentArray.Length * 2];

            for (int i = 0; i < currentArray.Length; i++)
            {
                newArray[i] = currentArray[i];
            }

            return newArray;
        }
    }
}
