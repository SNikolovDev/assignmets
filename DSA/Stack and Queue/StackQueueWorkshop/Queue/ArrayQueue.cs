﻿using System;
namespace StackQueueWorkshop.Queue
{
    public class ArrayQueue<T> : IQueue<T>
    {
        private T[] items;
        private int tail = 0;
        private int head = 0;
        private int size = 4;

        public ArrayQueue()
        {
            this.items = new T[size];
        }

        public int Size
        {
            get
            {
                return this.tail;
            }
        }

        public bool IsEmpty
        {
            get
            {
                return this.items.Length <= 0;
            }
        }

        public void Enqueue(T element)
        {
            if (this.Size == tail)
            {
                this.items = DoubleArraySize(this.items);
            }

            this.items[tail] = element;
            this.tail++;
        }

        public T Dequeue()
        {
            if (this.IsEmpty)
            {
                throw new InvalidOperationException();
            }

            var valueToReturn = this.items[head];
            this.head++;
            return valueToReturn;
        }

        public T Peek()
        {
            if (this.IsEmpty)
            {
                throw new InvalidOperationException();
            }

            return this.items[head];
        }

        public T[] DoubleArraySize(T[] currentArray)
        {
            var newArray = new T[currentArray.Length * 2];

            for (int i = 0; i < currentArray.Length; i++)
            {
                newArray[i] = currentArray[i];
            }

            return newArray;
        }
    }
}
