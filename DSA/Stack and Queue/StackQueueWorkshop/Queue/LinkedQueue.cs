﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace StackQueueWorkshop.Queue
{
    public class LinkedQueue<T> : IQueue<T>
    {
        private Node<T> head, tail;
        private int size;

        public int Size
        {
            get
            {
                return this.size;
            }
        }

        public bool IsEmpty
        {
            get
            {
                return this.size == 0;
            }
        }

        public void Enqueue(T element)
        {
            var node = new Node<T>();
            node.Data = element;

            if (this.head == null)
            {
                this.head = node;
                this.tail = this.head;
            }
            else if (this.size == 1)
            {
                this.tail = node;
                this.head.Next = tail;
            }
            else
            {
                this.tail.Next = node;
                this.tail = this.tail.Next;
            }

            this.size++;
        }

        public T Dequeue()
        {
            if (IsEmpty)
            {
                throw new InvalidOperationException();
            }

            var nodeToReturn = this.head;
            this.head = this.head.Next;
            this.size--;
            return nodeToReturn.Data;
        }

        public T Peek()
        {
            if (IsEmpty)
            {
                throw new InvalidOperationException();
            }

            return this.head.Data;
        }
    }
}
