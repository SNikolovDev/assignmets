﻿
namespace SearchingAndSorting.Sorting
{
    internal class BubbleSort
    {
        public static void Sort(int[] array)
        {
            bool swapped = true;
            while (swapped)
            {
                swapped = false;

                for (int i = 0; i < array.Length - 1; i++)
                {
                    if (array[i] > array[i + 1])
                    {
                        int temp = array[i];
                        array[i] = array[i + 1];
                        array[i + 1] = temp;
                        swapped = true;
                    }
                }
            }
        }
    }
}
