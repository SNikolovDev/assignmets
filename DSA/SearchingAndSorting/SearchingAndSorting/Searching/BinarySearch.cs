﻿namespace SearchingAndSorting.Searching
{
    internal class BinarySearch
    {
        public static bool ContainsRecursive(int[] array, int target, int leftIndex, int rightIndex)
        {
            if (leftIndex > rightIndex)
            {
                return false;
            }

            var middleIndex = (rightIndex - leftIndex) / 2;

            if (middleIndex == target)
            {
                return true;
            }

            if (array[middleIndex] > target)
            {
                return ContainsRecursive(array, target, leftIndex, rightIndex: middleIndex - 1);
            }
            else if (array[middleIndex] < target)
            {
                return ContainsRecursive(array, target, leftIndex: middleIndex + 1, rightIndex);
            }

            throw new ArgumentOutOfRangeException();
        }

        public static bool ContainsItterative(int[] array, int target)
        {
            int left = 0;
            int right = array.Length - 1;

            while (left <= right)
            {
                int middle = (left + right) / 2;

                if (array[middle] == target)
                {
                    return true;
                }

                if (array[middle] < target)
                {
                    left = middle + 1;
                }
                else if (array[middle] > target)
                {
                    right = middle - 1;
                }
            }

            return false;
        }
    }
}
