﻿using System;
using System.Collections.Generic;

namespace InHomeActivity.Map
{
    class Program
    {
        static void Main()
        {
        }

        static Dictionary<string, int> CountOccurences(string[] array)
        {
            var dictionary = new Dictionary<string, int>();
            foreach (var item in array)
            {
                if (!dictionary.ContainsKey(item))
                {
                    dictionary.Add(item, 0);
                }

                dictionary[item]++;
            }

            return dictionary;
        }

        static Dictionary<string, List<string>> Group(KeyValuePair<string, string>[] data)
        {
            var dictionary = new Dictionary<string, List<string>>();

            foreach (var kvp in data)
            {
                if (!dictionary.ContainsKey(kvp.Key))
                {
                    dictionary.Add(kvp.Key, new List<string>());
                }

                dictionary[kvp.Key].Add(kvp.Value);
            }

            return dictionary;
        }
    }
}
