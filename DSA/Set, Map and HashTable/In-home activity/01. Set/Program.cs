﻿using System.Collections.Generic;

namespace InHomeActivity.Set
{
    class Program
    {
        static void Main()
        {
        }

        static bool AreAllElementsUnique<T>(ICollection<T> collection)
        {
            var set = new HashSet<T>(collection);
            return set.Count == collection.Count;
        }
        static bool AreAllElementsUnique<T>(IEnumerable<T> collection)
        {
            var set = new HashSet<T>();

            foreach (var item in collection)
            {
                if (!set.Add(item))
                {
                    return false;
                }
            }

            return true;
        }
        static IEnumerable<T> Distinct<T>(IEnumerable<T> collection)
        {
            var set = new HashSet<T>(collection);
            return set;
        }
        static IEnumerable<T> Union<T>(IEnumerable<T> collection1, IEnumerable<T> collection2)
        {
            var set1 = new HashSet<T>(collection1);
            var set2 = new HashSet<T>(collection2);

            foreach (var item in set2)
            {
                set1.Add(item);
            }

            //alternative solution:
            //set1.UnionWith(set2);

            return set1;
        }
        static IEnumerable<T> Intersect<T>(IEnumerable<T> collection1, IEnumerable<T> collection2)
        {
            var set1 = new HashSet<T>(collection1);
            var set2 = new HashSet<T>(collection2);
            var intersection = new HashSet<T>();

            foreach (var item in set2)
            {
                if (set1.Contains(item))
                {
                    intersection.Add(item);
                }
            }

            return intersection;
        }
        static IEnumerable<T> Difference<T>(IEnumerable<T> collection1, IEnumerable<T> collection2)
        {
            var set1 = new HashSet<T>(collection1);
            var set2 = new HashSet<T>(collection2);
            var difference = new HashSet<T>();

            foreach (var item in set1)
            {
                if (!set2.Contains(item))
                {
                    difference.Add(item);
                }
            }

            return difference;
        }
    }
}
