﻿
namespace InventoryManager
{
    internal class Item
    {
        private string name;
        private double price;
        private string type;
        public static HashSet<Item> items = new HashSet<Item>();

        public Item(string name, double price, string type)
        {
            this.Name = name;
            this.Price = price;
            this.Type = type;
        }

        public string Name // unique
        {
            get => this.name;
            set
            {
                if (value.Length < 3 || value.Length > 15)
                {
                    throw new InvalidOperationException();
                }

                this.name = value;
            }
        }

        public double Price // postitive
        {
            get => this.price;
            set
            {
                if (value < 0)
                {
                    throw new InvalidOperationException();
                }

                this.price = value;
            }
        }

        public string Type // not unique
        {
            get => this.type;
            set
            {
                if (value.Length < 3 || value.Length > 15)
                {
                    throw new InvalidOperationException();
                }

                this.type = value;
            }
        }

        public void AddItem(Item item)
        {
            items.Add(item);
        }

        public static List<Item> FiltereItemsByType(string type)
        {
            return items.Where(i => i.type == type).ToList();
        }

        public static List<Item> FiltereItemsByPrice(double minPrice, double maxPrice)
        {
            return items.Where(i => i.price >= minPrice && i.price <= maxPrice).ToList();
        }

        public static List<Item> FiltereItemsByMinPrice(double minPrice)
        {
            return items.Where(i => i.price >= minPrice).ToList();
        }

        public static List<Item> FiltereItemsByMaxPrice(double maxPrice)
        {
            return items.Where(i => i.price >= maxPrice).ToList();
        }
    }
}
