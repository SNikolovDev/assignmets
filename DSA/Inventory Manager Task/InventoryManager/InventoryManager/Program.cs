﻿namespace InventoryManager
{
    internal class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                var input = Console.ReadLine();

                if (input == "end")
                {
                    break;
                }

                var data = input
                    .Split(' ')
                    .ToArray();

                var command = data[0];

                if (command == "add")
                {
                    var name = data[1];
                    var price = double.Parse(data[2]);
                    var type = data[3];
                    var item = new Item(name, price, type);

                    Item.items.Add(item);
                }
                else if (command == "filter")
                {
                    var filterBy = data[3];
                    var result = new List<Item>();

                    switch (filterBy)
                    {
                        case "type":
                            var type = data[3];
                            result = Item.FiltereItemsByType(type);
                            break;
                        case "price":
                            if (data.Length == 7)
                            {
                                var minPrice = double.Parse(data[5]);
                                var maxPrice = double.Parse(data[7]);
                                result = Item.FiltereItemsByPrice(minPrice, maxPrice);
                            }
                            else
                            {
                                var typeOfPrice = data[4];
                                var price = double.Parse(data[5]);

                                if (typeOfPrice == "from")
                                {
                                    result = Item.FiltereItemsByMinPrice(price);
                                }
                                else if (typeOfPrice == "to")
                                {
                                    result = Item.FiltereItemsByMaxPrice(price);
                                }
                            }
                            break;
                    }

                    PrintItems(result);
                }
            }
        }

        private static void PrintItems(List<Item> result)
        {
            foreach (var item in result)
            {
                Console.WriteLine($"{item.Name} {item.Price} {item.Type}");
            }

            Console.WriteLine("--------------------------------------");
        }
    }
}