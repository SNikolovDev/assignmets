﻿namespace Recursion
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(CountNumberOfDigits(12345, 0));
        }

        static void ReturnFirstNumbers(int count)
        {
            if (count == 0)
            {
                return;
            }

            ReturnFirstNumbers(count - 1);

            Console.WriteLine(count);
        }

        static void ReturnFromNumToOne(int num)
        {
            if (num == 0)
            {
                return;
            }

            Console.WriteLine(num);

            ReturnFromNumToOne(num - 1);
        }

        static int SumNaturalNumbers(int min, int val)
        {
            if (val == min)
            {
                return val;
            }
            return val + SumNaturalNumbers(min, val - 1);
        }

        static void SeparateDigits(int num)
        {
            if (num < 10)
            {
                Console.Write(num);
                return;
            }

            SeparateDigits(num / 10);

            Console.Write(num % 10);
        }

        static int CountNumberOfDigits(int num, int count)
        {
            if (num < 10)
            {
                return ++count;
            }

            return CountNumberOfDigits(num / 10, ++count);
        }

    }
}