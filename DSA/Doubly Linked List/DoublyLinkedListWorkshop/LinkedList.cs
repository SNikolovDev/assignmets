﻿using System;
using System.Collections;
using System.Reflection;
using System.Xml.Linq;


namespace DoublyLinkedListWorkshop
{
    public class LinkedList<T> : IList<T>
    {
        private Node head;
        private Node tail;

        public LinkedList()
        {
            this.head = this.tail = null;
            this.Count = 0;
        }

        public T Head
        {
            get
            {
                this.ThrowIfEmpty();
                return this.head.Value;
            }
        }

        public T Tail
        {
            get
            {
                this.ThrowIfEmpty();
                return this.tail.Value;
            }

        }

        public int Count
        {
            get;
            private set;
        }

        public void AddFirst(T value)
        {
            var node = new Node(value);

            if (this.Count == 0)
            {
                this.head = node;
                this.tail = node;
            }
            else
            {
                node.Next = this.head;
                this.head = node;
            }

            this.Count++;
        }

        public void AddLast(T value)
        {
            var node = new Node(value);

            if (this.Count == 0)
            {
                this.head = node;
                this.tail = node;
            }
            else
            {
                node.Prev = this.tail;
                this.tail.Next = node;
                this.tail = node;
            }

            this.Count++;
        }

        public void Add(int index, T value)
        {

            if (index < 0 || index > this.Count)
            {
                throw new ArgumentOutOfRangeException();
            }

            // We handle separately adding first or last
            if (index == 0)
            {
                this.AddFirst(value);
                return;
            }

            if (index == this.Count)
            {
                this.AddLast(value);
                return;
            }

            var node = FindNodeAtPosition(index);
            var prevNode = node.Prev;

            var newNode = new Node(value);
            prevNode.Next = newNode;
            newNode.Prev = prevNode;

            newNode.Next = node;
            node.Prev = newNode;
            this.Count++;
        }

        public T Get(int index)
        {
            this.ThrowIfEmpty();

            return FindNodeAtPosition(index).Value;
        }

        private Node GetNode(int index)
        {
            this.ThrowIfEmpty();

            return FindNodeAtPosition(index);
        }

        public int IndexOf(T value)
        {
            int result = 0;
            Node current = head;

            while (current != null)
            {
                if (current.Value.Equals(value))
                {
                    return result;
                }
                current = current.Next;
                result++;
            }

            return -1;
        }

        public T RemoveFirst()
        {
            this.ThrowIfEmpty();

            var removedValue = this.head.Value;

            if (this.Count == 1)
            {
                this.head = null;
                this.tail = null;
            }

            this.head = this.head.Next;
            this.head.Prev = null;
            this.Count--;

            return removedValue;
        }

        public T RemoveLast()
        {
            this.ThrowIfEmpty();

            var removedValue = this.tail.Value;

            if (this.Count == 1)
            {
                this.head = null;
                this.tail = null;               
            }

            this.tail = this.tail.Prev;
            this.tail.Next = null;
            this.Count--;

            return removedValue;
        }

        private Node FindNodeAtPosition(int index)
        {
            if (index < 0 || index >= this.Count)
            {
                throw new ArgumentOutOfRangeException();
            }

            Node current = this.head;
            while (index > 0)
            {
                current = current.Next;
                index--;
            }

            return current;
        }

        private void ThrowIfEmpty()
        {
            if (this.Count == 0)
            {
                throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Enumerates over the linked list values from Head to Tail
        /// </summary>
        /// <returns>A Head to Tail enumerator</returns>
        System.Collections.Generic.IEnumerator<T> System.Collections.Generic.IEnumerable<T>.GetEnumerator()
        {
            return new ListEnumerator(this.head);
        }

        /// <summary>
        /// Enumerates over the linked list values from Head to Tail
        /// </summary>
        /// <returns>A Head to Tail enumerator</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((System.Collections.Generic.IEnumerable<T>)this).GetEnumerator();
        }

        // Use private nested class so that LinkedList users
        // don't know about the LinkedList internal structure
        private class Node
        {
            public Node(T value)
            {
                this.Value = value;
            }

            public T Value
            {
                get;
                set;
            }

            public Node Next
            {
                get;
                set;
            }

            public Node Prev
            {
                get;
                set;
            }

            //public override string ToString()
            //{
            //    return $" {this.Value} ";
            //}
        }

        // List enumerator that enables traversing all nodes of a list in a foreach loop
        private class ListEnumerator : System.Collections.Generic.IEnumerator<T>
        {
            private Node start;
            private Node current;

            internal ListEnumerator(Node head)
            {
                this.start = head;
                this.current = null;
            }

            public T Current
            {
                get
                {
                    if (this.current == null)
                    {
                        throw new InvalidOperationException();
                    }
                    return this.current.Value;
                }
            }

            object IEnumerator.Current
            {
                get
                {
                    return this.Current;
                }
            }

            public void Dispose()
            {
            }

            public bool MoveNext()
            {
                if (current == null)
                {
                    current = this.start;
                    return true;
                }

                if (current.Next == null)
                {
                    return false;
                }

                current = current.Next;
                return true;
            }

            public void Reset()
            {
                this.current = null;
            }
        }
    }
}