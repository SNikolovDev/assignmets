﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using LinearDataStructures.Common;

namespace LinearDataStructures
{
    public static class LinearStructuresTasks
    {
        public static bool AreListsEqual<T>(SinglyLinkedList<T> list1, SinglyLinkedList<T> list2)
        {
            var node1 = list1.Head;
            var node2 = list2.Head;

            if (!node1.Value.Equals(node2.Value))
            {
                return false;
            }

            while (node1.Next != null && node2.Next != null)
            {
                if (node1.Next.Value.Equals(node2.Next.Value))
                {
                    node1 = node1.Next;
                    node2 = node2.Next;
                }
                else
                {
                    return false;
                }
            }

            if (node1.Next != node2.Next)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static Node<T> FindMiddleNode<T>(SinglyLinkedList<T> list)
        {
            var node = list.Head;
            var counter = 0;

            while (node.Next != null)
            {
                counter++;
                node = node.Next;
            }

            var middleNode = list.Head;

            for (int i = 0; i < Math.Ceiling((double)counter / 2); i++)
            {
                middleNode = middleNode.Next;
            }

            return middleNode;
        }

        public static SinglyLinkedList<T> MergeLists<T>(SinglyLinkedList<T> list1, SinglyLinkedList<T> list2) where T : IComparable
        {
            var merged = new SinglyLinkedList<T>();
            var node1 = list1.Head;
            var node2 = list2.Head;
            merged.AddFirst(node1.Value);
            var mergedNode = merged.Head;
            mergedNode.Next = node2;
            var current = mergedNode;

            while (node1.Next != null && node2.Next != null)
            {
                if (node1.Next != null)
                {
                    node1 = node1.Next;
                    current = node1;
                    mergedNode.Next = current;
                }
                if (node2.Next != null)
                {
                    node2 = node2.Next;
                    current = node2;
                    mergedNode.Next = current;
                }
            }

            return merged;
        }

        public static SinglyLinkedList<T> ReverseList<T>(SinglyLinkedList<T> list)
        {
            throw new NotImplementedException();

            //    var node = list.Head;
            //    var stack = new Stack<Node<T>>();

            //    while (node.Next != null)
            //    {
            //        stack.Push(node);
            //        node = node.Next;
            //    }

            //    var reversed = new SinglyLinkedList<Node<T>>();
            //    reversed.AddFirst(stack.Pop());
            //    var currentNode = reversed.Head;

            //    while (stack.Count > 0)
            //    {
            //        Node<T> nextNode = stack.Pop();
            //        currentNode.Next = nextNode;
            //        currentNode = currentNode.Next;
            //    }

            //    return reversed;
        }

    public static bool AreValidParentheses(string expression)
        {
            // 2 variations:

            //var left = new Stack<char>();
            //var right = new Stack<char>();

            //foreach (var symbol in expression)
            //{
            //    if (symbol == '(')
            //    {
            //        left.Push(symbol);
            //    }
            //    else if (symbol == ')')
            //    {
            //        right.Push(symbol);
            //    }
            //}

            //if (left.Count != right.Count)
            //{
            //    return false;
            //}

            //return true;

            char[] array = expression.ToCharArray();
            var stack = new Stack<char>();

            for (int i = 0; i < array.Length; i++)
            {
                char current = array[i];
                if (current == '(')
                {
                    stack.Push(current);
                }
                else if (current == ')')
                {
                    if (stack.Count == 0)
                    {
                        return false;
                    }

                    stack.Pop();
                }
            }

            if (stack.Count != 0)
            {
                return false;
            }

            return true;
        }

        public static string RemoveBackspaces(string sequence, char backspaceChar)
        {
            char[] array = sequence.ToCharArray();
            var stack = new Stack<char>();

            for (int i = 0; i < array.Length; i++)
            {
                char current = array[i];
                if (current != backspaceChar)
                {
                    stack.Push(current);
                }
                else if (current == backspaceChar)
                {
                    if (stack.Count == 0)
                    {
                        continue;
                    }

                    stack.Pop();
                }
            }

            var list = stack.ToArray().ToList();
            list.Reverse();

            return string.Join("", list);
        }
    }
}
