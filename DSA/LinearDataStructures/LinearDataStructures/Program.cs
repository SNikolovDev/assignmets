﻿using System;

using LinearDataStructures.Common;

namespace LinearDataStructures
{
    class Program
    {
        static void Main()
        {
            //var arr1 = new[] { 1, 2, 3, 4, 5 };
            //var arr2 = new[] { 1, 2, 3, 4, 5 };
            //var arr3 = new[] { 1, 2, 3 };

            //var list1 = new SinglyLinkedList<int>(arr1);
            //var list2 = new SinglyLinkedList<int>(arr2);
            //var list3 = new SinglyLinkedList<int>(arr3);

            //var result1 = LinearStructuresTasks.AreListsEqual(list1, list2);
            //var resutl2 = LinearStructuresTasks.AreListsEqual(list1, list3);

            //Console.WriteLine(result1);
            //Console.WriteLine(resutl2);

            //Console.WriteLine(LinearStructuresTasks.FindMiddleNode(list1).ToString());

            var sequence = "abc#d";
            Console.WriteLine(LinearStructuresTasks.RemoveBackspaces(sequence, '#'));
        }
    }
}
