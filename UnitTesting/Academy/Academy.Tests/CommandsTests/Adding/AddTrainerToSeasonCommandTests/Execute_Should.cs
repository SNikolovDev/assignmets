﻿using Academy.Commands.Adding;
using Academy.Commands.Contracts;
using Academy.Core;
using Academy.Models.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Academy.Tests.CommandsTests.Adding.AddTrainerToSeasonCommandTests
{
    [TestClass]
    public class Execute_Should
    {
        [TestMethod]
        public void Throw_When_ParametersAreLessThenExpected()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void Throw_When_SeasonIdIsNotInteger()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void Throw_When_TrainerDoesNotExist()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void Throw_When_SeasonDoesNotExist()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void Throw_When_TrainerAlreadyInSeason()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void ReturnMessage_When_ParametersAreCorrect()
        {
            throw new NotImplementedException();
        }

        private ICommand GetCommand(params string[] parameters)
        {
            Repository repository = this.GetRepository();
            IList<string> parametersList = new List<string>(parameters);
            ICommand command = new AddTrainerToSeasonCommand(parametersList, repository);
            return command;
        }

        private Repository GetRepository()
        {
            Repository repository = new Repository();

            var trainer = repository.CreateTrainer("trainer1", new List<string>());
            repository.CreateTrainer("trainer2", new List<string>());
            var season = repository.CreateSeason(2019, 2020, Initiative.SoftwareAcademy);
            season.Trainers.Add(trainer);

            return repository;
        }
    }
}
