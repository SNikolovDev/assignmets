﻿using Academy.Commands.Adding;
using Academy.Commands.Contracts;
using Academy.Core;
using Academy.Models.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Academy.Tests.CommandsTests.Adding.AddStudentToCourseCommandTests
{
    [TestClass]
    public class Execute_Should
    {
        [TestMethod]
        public void Throw_When_ParametersAreLessThenExpected()
        {
            //Arrange
            var sut = GetCommand("student", "1", "2");

            //Act, Assert
            Assert.ThrowsException<ArgumentException>(() => sut.Execute());
        }

        [TestMethod]
        public void Throw_When_SeasonIdIsNotInteger()
        {
            //Arrange
            var sut = GetCommand("student", "seasonId", "2", "form");

            //Act, Assert
            Assert.ThrowsException<ArgumentException>(() => sut.Execute());
        }

        [TestMethod]
        public void Throw_When_CourseIdIsNotInteger()
        {
            //Arrange
            var sut = GetCommand("student", "1", "courseId", "form");

            //Act, Assert
            Assert.ThrowsException<ArgumentException>(() => sut.Execute());
        }

        [TestMethod]
        public void Throw_When_StudentDoesNotExist()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void Throw_When_SeasonDoesNotExist()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void Throw_When_CourseDoesNotExist()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void Throw_When_FormIsInvalid()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void ReturnMessage_When_ParametersAreCorrect()
        {
            throw new NotImplementedException();
        }

        private ICommand GetCommand(params string[] parameters)
        {
            Repository repository = this.GetRepository();
            IList<string> parametersList = new List<string>(parameters);
            ICommand command = new AddStudentToCourseCommand(parametersList, repository);
            return command;
        }

        private Repository GetRepository()
        {
            Repository repository = new Repository();

            repository.CreateStudent("student", Track.Dev);
            var season = repository.CreateSeason(2019, 2020, Initiative.SoftwareAcademy);
            var course = repository.CreateCourse("C# OOP", 5, DateTime.Parse("2019-09-15"), DateTime.Parse("2020-03-03"));
            season.Courses.Add(course);

            return repository;
        }
    }
}
