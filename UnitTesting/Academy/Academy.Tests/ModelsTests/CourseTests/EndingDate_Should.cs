﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Newtonsoft.Json.Linq;

using System;

namespace Academy.Tests.ModelsTests.CourseTests
{
    [TestClass]
    public class EndingDate_Should
    {
        [TestMethod]
        public void Throw_When_ValueIsNull()
        {
            //Arrange
            var sut = Utils.Helpers.GetCourse();

            //Act & Assert
            Assert.ThrowsException<ArgumentNullException>(() => sut.EndingDate = null);
        }

        [TestMethod]
        public void ShouldSetEndingDateIfValidParameters()
        {
            var sut = Utils.Helpers.GetCourse();

            sut.EndingDate = new DateTime(2023, 2, 23);

            Assert.AreEqual(sut.EndingDate, new DateTime(2023, 2, 23));
        }
    }
}
