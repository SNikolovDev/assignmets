﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Academy.Tests.ModelsTests.CourseTests
{
    [TestClass]
    public class Name_Should
    {
        [TestMethod]
        [DataRow(null)]
        [DataRow("")]
        [DataRow("aaaa")]
        [DataRow("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")]
        public void ShouldThrowExceptionIfInvalidName(string value)
        {
            var sut = Utils.Helpers.GetCourse();

            Assert.ThrowsException<ArgumentException>(() => sut.Name = value);
        }

        [TestMethod]
        public void ChangeValue_When_ValueIsCorrect()
        {
            //Arrange
            var sut = Utils.Helpers.GetCourse();

            //Assert
            Assert.AreEqual(sut.Name, "Alpha C#");
        }
    }
}
