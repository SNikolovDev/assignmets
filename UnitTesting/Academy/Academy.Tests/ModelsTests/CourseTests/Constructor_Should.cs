﻿using Academy.Models.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Academy.Tests.ModelsTests.CourseTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void InitializeOnlineStudents()
        {
            //Arrange, Act
            var sut = Utils.Helpers.GetCourse();

            //Assert
            Assert.IsInstanceOfType(sut.OnlineStudents, typeof(List<IStudent>));
        }

        [TestMethod]
        public void InitializeOnsiteStudents()
        {
            //Arrange, Act
            var sut = Utils.Helpers.GetCourse();

            //Assert
            Assert.IsInstanceOfType(sut.OnsiteStudents, typeof(List<IStudent>));
        }

        [TestMethod]
        public void InitializeLectures()
        {
            var sut = Utils.Helpers.GetLecture();

            Assert.IsInstanceOfType(sut, typeof(ILecture));
        }

        [TestMethod]
        public void SetNameIfValidParameter()
        {
            var sut = Utils.Helpers.GetCourse();

            Assert.AreEqual(sut.Name, "Alpha C#");
        }
    }
}
