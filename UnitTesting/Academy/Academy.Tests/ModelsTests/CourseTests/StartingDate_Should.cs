﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Academy.Tests.ModelsTests.CourseTests
{
    [TestClass]
    public class StartingDate_Should
    {
        [TestMethod]
        public void Throw_When_ValueIsNull()
        {
            var sut = Utils.Helpers.GetCourse();

            Assert.ThrowsException<ArgumentNullException>(() => sut.StartingDate = null);
        }

        [TestMethod]
        public void ChangeValue_When_ValueIsCorrect()
        {
            //Arrange
            var sut = Utils.Helpers.GetCourse();
            var expected = DateTime.Parse("10.10.2010");

            //Act
            sut.StartingDate = expected;

            //Assert
            Assert.AreEqual(expected, sut.StartingDate);
        }
    }
}
