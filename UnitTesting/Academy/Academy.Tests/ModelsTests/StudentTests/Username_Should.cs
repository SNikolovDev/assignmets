﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Academy.Tests.ModelsTests.StudentTests
{
    [TestClass]
    public class Username_Should
    {
        [TestMethod]
        public void Throw_When_ValueIsNullOrWhiteSpace()
        {
            //Arrange
            var student = Utils.Helpers.GetStudent();
            string newUsername = null;

            //Act
            Assert.ThrowsException<ArgumentException>(() => student.Username = newUsername);
        }

        [TestMethod]
        public void Throw_When_ValueIsLessThanMin()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void Throw_When_ValueIsLargerThanMax()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void ChangeValue_When_ValueIsCorrect()
        {
            throw new NotImplementedException();
        }
    }
}