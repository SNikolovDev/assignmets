﻿using Academy.Models;
using Academy.Models.Contracts;
using Academy.Tests.Utils;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Academy.Tests.ModelsTests.LectureTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void InitializeResources()
        {
            var sut = Helpers.GetLecture();

            Assert.IsInstanceOfType(sut, typeof(Lecture));
        }
    }
}
