﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Academy.Tests.ModelsTests.LectureTests
{
    [TestClass]
    public class Name_Should
    {
        [TestMethod]
        public void Throw_When_ValueIsNull()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void Throw_When_ValueIsLessThanMinValue()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void Throw_When_ValueIsLargerThanMaxValue()
        {
            //Arrange
            var sut = Utils.Helpers.GetLecture();

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => sut.Name = new string('a', 31));
        }

        [TestMethod]
        public void ChangeValue_When_ValueIsCorrect()
        {
            throw new NotImplementedException();
        }
    }
}
