﻿using Boarder.Models;

namespace Boarder.Tests.ModelTests
{
    [TestClass]
    internal class IssueTests
    {
        [TestMethod]
        public void ShouldCreateIssueWhenValidParameters()
        {
            string title = "Title";
            string description = "Test";
            DateTime dueDate = DateTime.Parse("20-03-2030");

            Issue task = new Issue(title, description, dueDate);

            Assert.AreEqual("Title", task.Title);
        }
    }
}
