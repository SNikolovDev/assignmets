﻿using Boarder.Models;

using Task = Boarder.Models.Task;

namespace Boarder.Tests.ModelTests
{
    [TestClass]
    public class TaskTests
    {
        [TestMethod]
        public void ShouldCreateTaskWhenParamertersAreValid()
        {
            string title = "Title";
            string assignee = "Assignee";
            DateTime dueDate = DateTime.Parse("20-03-2030");

            Task task = new Task(title, assignee, dueDate);

            Assert.AreEqual("Title", task.Title);
        }

        [TestMethod]
        public void ShouldSetAssigneeWhenParamertersAreValid()
        {
            Task task = new Task("Title", "Assignee", DateTime.Parse("20-03-2030"));

            task.Assignee = "Second Assignee";

            Assert.AreEqual("Second Assignee", task.Assignee);
        }

        [TestMethod]
        public void ShouldSetAdvanceStatusIfNotAlreadyAtVerified()
        {
            Task task = new Task("Title", "Assignee", DateTime.Parse("20-03-2030"));

            task.AdvanceStatus();

            Assert.AreEqual(Status.InProgress, task.Status);
        }

        [TestMethod]
        public void ShouldAddToLogIfStatusAlreadyAtVerified()
        {
            Task task = new Task("Title", "Assignee", DateTime.Parse("20-03-2030"));

            task.AdvanceStatus();
            task.AdvanceStatus();
            task.AdvanceStatus();
            task.AdvanceStatus();

        }

        [TestMethod]
        public void ShouldSetAdvanceRevertIfNotAlreadyAtToDo()
        {
            Task task = new Task("Title", "Assignee", DateTime.Parse("20-03-2030"));

            task.AdvanceStatus();
            task.RevertStatus();

            Assert.AreEqual(Status.Todo, task.Status);
        }

        [TestMethod]
        [DataRow(null)]
        [DataRow(" ")]
        public void ShouldThrowExceptionIfStringIsNullOrWhiteSpace(string value)
        {
            Assert.ThrowsException<ArgumentException>(() =>
             new Task("Test title", value, DateTime.Parse("20-03-2030")));
        }

        [TestMethod]
        [DataRow("a")]
        [DataRow("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")]
        public void ShouldThrowExceptionIfStringIsLessThan5OrMoreThan30(string value)
        {
            Assert.ThrowsException<ArgumentException>(() =>
             new Task("Test title", value, DateTime.Parse("20-03-2030")));
        }

        [TestMethod]
        public void ShouldAddToEventLogWhenAlreadyAtToDo()
        {
            Task task = new Task("Title", "Assignee", DateTime.Parse("20-03-2030"));

            task.RevertStatus();

            Assert.AreEqual(expected: 2, GetEventLogCount(task));
        }

        private int GetEventLogCount(BoardItem item)
        {
            return item.ViewHistory().Split(Environment.NewLine).Length;
        }
    }
}

